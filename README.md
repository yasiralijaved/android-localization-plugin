# Android Localization Plugin

This is a gradle plugin for android that can generate the strings.xml file from a Google Spreadsheet. This plugin is flexible as it is using configuration approach. Developers can generate as many different strings.xml files for different translation as they can.

# How to use

1- Add plugin into your app's build.gradle file by following the steps from here https://plugins.gradle.org/plugin/com.yasiralijaved.localizationplugin 

2- Now create a block named "localizations" in the same apps' build.gradle file

	//localization extension from com.yasiralijaved.localizationplugin
	localizations {
	
	}

3- Now add your first configuration into this block

	//localization extension from com.yasiralijaved.localizationplugin
	localizations {
		localizationEN {
			taskTitle = 'My EN Localization Task'
			googleSpreadsheetUrl = 'https://docs.google.com/spreadsheets/d/....'
			stringsXmlFilePath = 'src/main/res/values/strings.xml'
			startingRowNumber = 1
			translationColumnNumber = 1
		}
	}

![picture](Screenshots/sheet_en.png)

4-	Now after refreshing the Gradle you will see a new gradle task group called "localization plugin" under "Tasks" in the Gradle Tool Window

![picture](Screenshots/gradle_tool_window.png)

5- Under group "localization plugin" you will see a new gradle task with name "localizationEN". 

6- By double clicking on this task gradle will download, convert into csv and then will write the data into src/main/res/values/strings.xml file

7- To see the log of this executed Task you can see the Gradle Console

# Multi-configuration
You can create multiple configurations (similar to configuration localizationEN) as many as you want. Corresponding Gradle Tasks for each configuration will be available in task group "localization plugin"

![picture](Screenshots/sheet_multi_config.png)

	//localization extension from com.yasiralijaved.localizationplugin
	localizations {
		localizationEN {
			taskTitle = 'My EN Localization Task'
			googleSpreadsheetUrl = 'https://docs.google.com/spreadsheets/d/....'
			stringsXmlFilePath = 'src/main/res/values/strings.xml'
			startingRowNumber = 1
			translationColumnNumber = 1
		}
		localizationAR {
			taskTitle = 'My AR Localization Task'
			googleSpreadsheetUrl = 'https://docs.google.com/spreadsheets/d/....'
			stringsXmlFilePath = 'src/main/res/values-ar/strings.xml'
			startingRowNumber = 1
			translationColumnNumber = 2
		}
		DownloadTranslationFrench {
			taskTitle = 'My French Localization Task'
			googleSpreadsheetUrl = 'https://docs.google.com/spreadsheets/d/....'
			stringsXmlFilePath = 'src/main/res/values-fr/strings.xml'
			startingRowNumber = 1
			translationColumnNumber = 3
		}
	}


![picture](Screenshots/gradle_tool_multi_config.png)
